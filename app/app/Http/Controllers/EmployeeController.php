<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $emp = DB::table('Employee')->get();
        $employee = DB::table('Department')->join('Position','departm_dep_id','=','Department.id')->join('Employee','position_pos_id','=','Position.id')->orderBy('emp_name')->get();
        //dd($employee);
        return view('employee.index',compact('employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Department = DB::table('Department')->get();
        $Position = DB::table('Position')->get();
        //dd($Department);
        return view('employee.create',compact('Department','Position'));
    }
    public function fetch(Request $request)
    {
        $id= $request->get('select');
        $result = array();
        $query=DB::table('Department')
        ->join('Position','Department.id','=','Position.departm_dep_id')
        ->select('Position.id','Position.pos_name')
        ->where('Department.id',$id)
        ->groupBy('Position.id')
        ->get();
        $output ='<option value="">เลือกตำแหน่ง</option>';
        foreach($query as $row){
           
            $output.='<option value="'.$row->id.'"  >'.$row->pos_name.'</option>';
        }
        echo $output;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate(
        //     [
        //         'ACC_NO' => 'required|max:10',
        //         'ACC_NAME' => 'required|max:50',
        //         'ACC_Surname' => 'required|max:100',
        //         'Balance' => 'required'
        //     ]
        // );
        $employee = new Employee;
        $employee->emp_name = $request->get('emp_name');
        $employee->emp_surName = $request->get('emp_surName');
        $employee->emp_img = $request->get('emp_img');
        $employee->emp_sex = $request->get('emp_sex');
       
        $employee->emp_phone = $request->get('emp_phone');
        $employee->emp_email = $request->get('emp_email');
        $employee->emp_birthday = $request->get('emp_birthday');
        $employee->emp_nationality = $request->get('emp_nationality');
        $employee->emp_qualification = $request->get('emp_qualification');
        $employee->position_pos_id = $request->get('position_pos_id');
       
        $employee->save();

        return redirect('employee');
    }
    public function storeDep(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee=Employee::find($id);
        
        return view('employee.edit',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = employee::find($id);
        // $request->validate(
        //     [
        //         'ACC_No' => 'required|max:10',
        //         'ACC_Name' => 'required|max:0',
        //         'ACC_Sername' => 'required|max:10',
        //         'ACC_Balance' => 'required'
        //     ]
        // );
        $employee->emp_name =  $request->emp_name;
        $employee->emp_surName = $request->emp_surName;    
        $employee->emp_sex = $request->emp_sex;
        $employee->emp_email = $request->emp_email;
        $employee->emp_phone = $request->emp_phone;
        $employee->emp_birthday = $request->emp_birthday;
        $employee->emp_nationality = $request->emp_nationality;
        $employee->emp_qualification = $request->emp_qualification;
        $employee->position_pos_id = $request->position_pos_id;
        $employee->update();
        return redirect("employee");
        // $employee=Employee::find($id);
        // $employee = new Employee;
        // $employee->emp_name = $request->get('emp_name');
        // $employee->emp_surName = $request->get('emp_surName');
        // $employee->emp_img = $request->get('emp_img');
        // $employee->emp_sex = $request->get('emp_sex');
       
        // $employee->emp_phone = $request->get('emp_phone');
        // $employee->emp_email = $request->get('emp_email');
        // $employee->emp_birthday = $request->get('emp_birthday');
        // $employee->emp_nationality = $request->get('emp_nationality');
        // $employee->emp_qualification = $request->get('emp_qualification');
        // $employee->position_pos_id = $request->get('position_pos_id');
       
        // $employee->save();

        // return redirect('employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee =Employee::find($id);
        $employee->delete();
        return redirect("employee");
    }
}
