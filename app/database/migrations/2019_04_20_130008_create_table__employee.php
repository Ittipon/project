<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Employee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('emp_name',100);
            $table->char('emp_surName',100);
            $table->binary('emp_img');
            $table->char('emp_sex',10);
            $table->char('emp_phone',15);
            $table->char('emp_email',45);
            $table->date('emp_birthday');
            $table->char('emp_nationality',45);
            $table->char('emp_qualification',45);
            $table->unsignedBigInteger('position_pos_id');
            $table->foreign('position_pos_id')->references('id')->on('Position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Employee');
    }
}
