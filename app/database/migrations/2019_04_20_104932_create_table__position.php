<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Position', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('pos_name',100);
            $table->unsignedBigInteger('departm_dep_id');
            $table->foreign('departm_dep_id')->references('id')->on('Department');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Position');
    }
}
