<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployeeHasTraningHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Employee_has_traning_history', function (Blueprint $table) {
            $table->unsignedBigInteger('Employee_emp_id');
            $table->foreign('Employee_emp_id')->references('id')->on('Employee');
            $table->unsignedBigInteger('traning_history_tran_id');
            $table->foreign('traning_history_tran_id')->references('id')->on('Traning_history');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Employee_has_traning_history');
    }
}
