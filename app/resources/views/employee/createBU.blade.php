
<html>
    <head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
    
    
                    <div class="panel-heading">
                    <form method="post" action="{{ route('employee.store') }}">
                            {{ csrf_field() }}
                        <form>
                        <!-- <div class="form-group">
                            <label for="emp_id">id</label>
                            <input type="text" class="form-control" id="formGroupExampleInput" name="emp_id" placeholder="Example input">
                        </div> -->
                        <div class="form-group">
                        <label for="emp_name">Name</label>
                        <input type="text" class="form-control"   name="emp_name" splaceholder="Another input">
                        </div>
                        <div class="form-group">
                        <label for="emp_surName">Surname</label>
                        <input type="text" class="form-control"   name="emp_surName" splaceholder="Another input">
                        </div>
                        <div class="custom-file">
                         <input type="file" name="emp_img" class="custom-file-input" id="customFile">
                         <label class="custom-file-label" for="emp_img">Choose file</label>
                        </div>
                        <div class="form-group">
                        <label for="emp_sex">sex</label>
                        <input type="text" class="form-control"   name="emp_sex" splaceholder="Another input">
                        </div>
                        <div class="form-group">
                        <label for="emp_email">email</label>
                        <input type="text" class="form-control"   name="emp_email" splaceholder="Another input">
                        </div>
                        <div class="form-group">
                        <label for="emp_phone">phone</label>
                        <input type="text" class="form-control"   name="emp_phone" splaceholder="Another input">
                        </div>
                        <div class="form-group">
                        <label for="emp_birthday">birthday</label>
                        <input type="text" class="form-control"   name="emp_birthday" splaceholder="Another input">
                        </div>
                        <div class="form-group">
                        <label for="emp_nationality">nationality</label>
                        <input type="text" class="form-control"   name="emp_nationality" splaceholder="Another input">
                        </div>
                        <div class="form-group">
                        <label for="emp_qualification">qualification</label>
                        <input type="text" class="form-control"   name="emp_qualification" splaceholder="Another input">
                        </div>
                        <div class="form-group">
                        <label for="position_pos_id">id</label>
                        <input type="text" class="form-control"   name="position_pos_id" splaceholder="Another input">
                        </div>
                        <div class="dropdown">
                        <label >Department</label>
    
                        <select id="province"class="form-control form-control-sm province">
                            <option >เลือกแผนก</option>
                        @foreach($Department as $row)
                         <option value="{{ $row->id }}">{{ $row->dep_name }}</option>
                        @endforeach 
                        </select>
                      
                        <label >Position</label>
                        <select class="form-control form-control-sm position">
                                <option >เลือกตำแหน่ง</option>
                        
                     
                        </select>
                      
                    </form>
    
                        <div>
                            <button type="submit" class="btn btn-default">เพิ่ม</button>                    
                        </div>
                    
                    </form>
                    </div>
                   
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    </body>
    <script type="text/javascript">
                            
                    $(".province").change(function(){
                       if($(this).val()!=''){ 
                        var select=$(this).val();
                        var _token=$('input[name="token"]').val();
                        $.ajax({
                            
                            headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              },
                            url:"{{route('dropdown.fetch')}}",
                            method:"POST",
                            data:{select:select,_token:_token},
                            success:function(result){
                                //เสร็จแล้วทำอะไรต่อ
                                $('.Position').html(result);
                            }
                        
                        })
                       }
                    });
                    </script>  
    </head>
    </html>
    
    