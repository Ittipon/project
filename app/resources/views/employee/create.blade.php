<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Hello, world!</title>
    <style media="Screnn">
 
    </style>
  
  <body class="bg-light ">
        <nav class="navbar navbar-light " style="background-color: #FFFFFF;">
                <div class="container">
                        <a class="navbar-brand" href="http://localhost" >
                            Laravel
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
        
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav mr-auto">
        
                            </ul>
        
                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                                                            <li class="nav-item">
                                        <a class="nav-link" href="http://localhost/login">Login</a>
                                    </li>
                                                                    <li class="nav-item">
                                            <a class="nav-link" href="http://localhost/register">Register</a>
                                        </li>
                                                                                </ul>
                        </div>
                    </div>
              </nav>
    <main class="py-4 bg-light">
    <div class="container box">
        <a  class="btn btn-success" href="{{ route('employee.index') }}">หน้าหลัก</a><br><br>

        <form method="post" action="{{ route('employee.store') }}">
                {{ csrf_field() }}
        
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="emp_name">name</label>
                        <input type="text" class="form-control" name="emp_name" placeholder="name">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="emp_surName">Lastname</label>
                        <input type="text" class="form-control" name="emp_surName" placeholder="Last name">
                    </div>
                    <div class="form-group col-md-6">
                            <label >sex</label>
                        <select class="form-control form-control-sm " name="emp_sex">
                                <option value="female" >female</option>
                                <option value="male" >male</option>
                        </select>
                        </div>
                </div>    
                {{-- <div class="form-group">
                        <label for="emp_sex">sex</label>
                        <input type="text" class="form-control"   name="emp_sex" splaceholder="Another input">
                </div> --}}
                {{-- <div class="form-row">
                <div class="form-group">
                        <label >sex</label>
                        <select class="form-control form-control-sm position" name="emp_sex">
                                <option value="female" >female</option>
                                <option value="male" >male</option>
                        </select>
                </div>       
                </div> --}}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="emp_email">Email</label>
                        <input type="email" class="form-control" name="emp_email"  placeholder="email@hotmail.com">
                    </div>
                    <div class="form-group col-md-6">
                            <label for="emp_phone">Phone</label>
                            <input type="text" class="form-control" name="emp_phone" placeholder="09222222222">
                    </div>        
                </div> 
                <div class="form-group">
                        <label for="emp_birthday">birthday</label>
                        
                          <input class="form-control" type="date" name="emp_birthday" id="example-date-input">
                </div>
                <div class="form-group">
                        <label for="emp_nationality">nationality</label>
                        <input type="text" class="form-control"   name="emp_nationality" splaceholder="Another input">
                </div>
                <div class="form-group">
                        <label for="emp_qualification">qualification</label>
                        <input type="text" class="form-control"   name="emp_qualification" splaceholder="Another input">
                </div>
                           
                <div class="form-row">
                        <div class="form-group col-md-6">
                                <label >Department</label>
    
                                <select id="province"class="form-control form-control-sm province">
                                    <option >เลือกแผนก</option>
                                @foreach($Department as $row)
                                 <option value="{{ $row->id }}" >{{ $row->dep_name }}</option>
                                 
                                @endforeach 
                                </select>
                        </div>
                        <div class="form-group col-md-6">
                                <label >Position</label>
                                <select class="form-control form-control-sm position" name="position_pos_id">
                                        <option >เลือกตำแหน่ง</option>
                                </select>
                        </div>        
                    </div> 
                    
                    <div>
                            <button type="submit" class="btn btn-default">เพิ่ม</button>                    
                        </div>
       


      </form>
      <form method="post" action="{{ route('department.store') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="dep_name">department</label>
            <input type="text" class="form-control"   name="dep_name" splaceholder="Another input">
            <div>
                <button type="submit" class="btn btn-default">เพิ่มแผนก</button>                    
            </div>
    </div>
      </form>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript">
                  
        $(".province").change(function(){
           if($(this).val()!=''){ 
            var select=$(this).val();
            var _token=$('input[name="token"]').val();
            $.ajax({
                
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                url:"{{route('dropdown.fetch')}}",
                method:"POST",
                data:{select:select,_token:_token},
                success:function(result){
                    //เสร็จแล้วทำอะไรต่อ
                    $('.position').html(result);
                    console.log(result)
                }
            
            })
           }
        });
        </script>  
    <main>
</body>
</head>
</html>