<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('employee','EmployeeController');
Route::resource('department','DepartmentController');

Auth::routes();
Route::post('employee/create/fetch','EmployeeController@fetch')->name('dropdown.fetch');
Route::get('/home', 'HomeController@index')->name('home');

//Route::post('/province/fetch','DropdownController@fetch')->name('dropdown.fetch');

